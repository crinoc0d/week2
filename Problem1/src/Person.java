public class Person {
    private String[] firstName;
    private String surName;

    Person(String[] firstName, String surName) {
        this.firstName = firstName;
        this.surName = surName;
    }

    Person(String fullName) {
        String[] splitFullName = fullName.split(" ");
        surName = splitFullName[splitFullName.length - 1];

        firstName = new String[splitFullName.length - 1];
        for (int i = 0; i < splitFullName.length - 1; i++) {
            firstName[i] = splitFullName[i];
        }
    }

    void printProperties() {
        System.out.print("firstName = ");
        for (String s: firstName) {
            System.out.print(s + " ");
        }
        System.out.println(", surName = " + surName);
    }

    public static void main(String[] args) {
        Person person1 = new Person(new String[] {"Alina", "Costina"}, "Nitu");
        person1.printProperties();

        Person person2 = new Person("Patricia Mihaela Stanca");
        person2.printProperties();
    }
}
