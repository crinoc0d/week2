public class Tank {
    private boolean[] liters = new boolean[10]; // if litter[i] is true, i + 1 liters are in the tank
    private int size = 0;
    private static int counterTries;

    void pushTank() {
        if (size == 10) {
            System.out.println("The tank is full.");
        } else {
            liters[size] = true;
            size++;
            System.out.println("Tank has " + size + " liters.");
        }
    }

    void popTank() {
        if (size == 0) {
            System.out.println("The tank is empty.");
        } else {
            size--;
            liters[size] = false;
            System.out.println("Tank has " + size + " liters.");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (liters[0] == false) {
            System.out.println("The object " + this + " can be destroyed during Try no. " + Tank.counterTries + ". The tank is empty.");
            /*for (int i = 0; i < 10; i++) {
                System.out.print(liters[i] + " ");
            }*/
       }
    }

    public static void main(String[] args) {
        while (true) { // endless loop to catch the garbage collection cleaning up
            Tank tank = new Tank();

            System.out.println("Try no. " + Tank.counterTries + ", object " + tank);
            for (int i = 1; i <= 15; i++) {
                tank.pushTank();
            }
            for (int i = 1; i <= 15; i++) {
                tank.popTank();
            }
            System.gc();
            Tank.counterTries++;
        }
    }
}
