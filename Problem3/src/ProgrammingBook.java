public class ProgrammingBook extends ScienceBook {
    static int noProgrammingBooks = 12;
    static {
        System.out.println("3. ScienceBook class static initialization");
        System.out.println("   Default initialization, noScienceBooks = " + noProgrammingBooks);
        noProgrammingBooks = 18;
        System.out.println("   After static block init, noScienceBooks = " + noProgrammingBooks);
    }
    private String language;

    ProgrammingBook() {
        System.out.println("7. ProgrammingBook() constructor was called.");
        System.out.println("   Before constructor initialization: language = " + language + ", noProgrammingBooks = " + noProgrammingBooks);
        language = "assembling";
        noProgrammingBooks++;
        System.out.println("   Constructor initialization: language = " + language + ", noProgrammingBooks = " + noProgrammingBooks);
    }

    ProgrammingBook(int barcode, String author, String title, String domain, String language) {
        super(barcode, author, title, domain);
        System.out.println("10. ProgrammingBook(barcode, author, title, domain, language) constructor was called.");
        System.out.println("    Before constructor initialization: language = " + this.language + ", noProgrammingBooks = " + noProgrammingBooks);
        this.language = language;
        noProgrammingBooks++;
        System.out.println("    Constructor initialization: language = " + this.language + ", noProgrammingBooks = " + noProgrammingBooks);
    }

    public static void main(String[] args) {
        System.out.println("4. main was called.");
        ProgrammingBook book1 = new ProgrammingBook(),
                book2 = new ProgrammingBook(654321, "Bruce Eckel", "Thinking in Java", "Computer Science", "Java");

        System.out.println("11. No. of books = " + Book.noBooks);
        System.out.println("    No. of science books = " + ScienceBook.noScienceBooks);
        System.out.println("    No. of programming books = " + ProgrammingBook.noProgrammingBooks);
    }
}
