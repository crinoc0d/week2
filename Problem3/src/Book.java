public class Book {
    static int noBooks;
    static {
        System.out.println("1. Book class static initialization");
        System.out.println("   Default initialization, noBooks = " + noBooks);
        noBooks = 1;
        System.out.println("   After static block init, noBooks = " + noBooks);
    }
    private int barcode;
    private String author;
    private String title;

    Book() {
        System.out.println("5. Book() constructor was called.");
        System.out.println("   Before constructor initialization: barcode = " + barcode
                + ", author = " + author
                + ", title = " + title
                + ", noBooks = " + noBooks);
        barcode = 123456;
        author = "John Doe";
        title = "No title";
        noBooks++;
        System.out.println("   Constructor initialization: barcode = " + barcode
                + ", author = " + author
                + ", title = " + title
                + ", noBooks = " + noBooks);
    }
    Book(int barcode, String author, String title) {
        System.out.println("8. Book(barcode, author, title) constructor was called.");
        System.out.println("   Before constructor initialization: barcode = " + this.barcode
                + ", author = " + this.author
                + ", title = " + this.title
                + ", noBooks = " + noBooks);
        this.barcode = barcode;
        this.author = author;
        this.title = title;
        noBooks++;
        System.out.println("   Constructor initialization: barcode = " + this.barcode
                + ", author = " + this.author
                + ", title = " + this.title
                + ", noBooks = " + noBooks);
    }
}
