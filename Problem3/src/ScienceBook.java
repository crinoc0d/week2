public class ScienceBook extends Book {
    static int noScienceBooks = 5;
    static {
        System.out.println("2. ScienceBook class static initialization");
        System.out.println("   Default initialization, noScienceBooks = " + noScienceBooks);
        noScienceBooks = 10;
        System.out.println("   After static block init, noScienceBooks = " + noScienceBooks);
    }
    private String domain;

    ScienceBook() {
        System.out.println("6. ScienceBook() constructor was called.");
        System.out.println("   Before constructor initialization: domain = " + domain + ", noScienceBooks = " + noScienceBooks);
        domain = "General";
        noScienceBooks++;
        System.out.println("   Constructor initialization: domain = " + domain + ", noScienceBooks = " + noScienceBooks);
    }

    ScienceBook(int barcode, String author, String title, String domain) {
        super(barcode, author, title);
        System.out.println("9. ScienceBook(barcode, author, title, domain) constructor was called.");
        System.out.println("   Before constructor initialization: domain = " + this.domain + ", noScienceBooks = " + noScienceBooks);
        this.domain = domain;
        noScienceBooks++;
        System.out.println("   Constructor initialization: domain = " + this.domain + ", noScienceBooks = " + noScienceBooks);
    }
}
